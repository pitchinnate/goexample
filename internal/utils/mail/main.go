package mail

import "gopkg.in/mailgun/mailgun-go.v1"

type EmailMessage struct {
	To      string
	From    string
	Subject string
	Body    string
}

func setupMailer() mailgun.Mailgun {
	var yourDomain string = "mg.meetup.com"
	var privateAPIKey string = "4c348fbc8d1968ff330e2e81dd13c1ce-c9270c97-16c24e50"
	var publicValidationKey string = "pubkey-2e8203071c3b96f0188742936a447489"

	return mailgun.NewMailgun(yourDomain, privateAPIKey, publicValidationKey)
}

func SendEmail(message EmailMessage) error {
	mg := setupMailer()
	newMessage := mg.NewMessage(message.From, message.Subject, "Please view in HTML", message.To)
	newMessage.SetHtml(message.Body)
	_, _, err := mg.Send(newMessage)
	return err
}
