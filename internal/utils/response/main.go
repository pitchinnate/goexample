package response

import (
	"meetup/internal/pkg/models"
	errors2 "meetup/internal/utils/errors"
	"encoding/json"
	"net/http"
	"time"
)

type TextResponse struct {
	Message string `json:"message"`
}

type ApiResponse struct {
	Meta   interface{} `json:"meta"`
	Data   interface{} `json:"data"`
	Errors []string    `json:"errors"`
}

type MetaResponse struct {
	Version     string    `json:"version"`
	RequestedAt time.Time `json:"requestedAt"`
}

type ResponseData struct {
	User     interface{}
	Data     interface{}
	Errors   []error
	Request  *http.Request
	Response http.ResponseWriter
	Status   int
}

func GenerateResponseData(w http.ResponseWriter, r *http.Request) *ResponseData {
	user := r.Context().Value("user")

	var emptyInterface interface{}
	var responseData ResponseData
	responseData.Request = r
	responseData.Response = w
	responseData.Status = 0
	responseData.Data = emptyInterface
	responseData.Errors = []error{}
	responseData.User = user
	return &responseData
}

func (responseData *ResponseData) AddError(err error) {
	if err != nil {
		responseData.Errors = append(responseData.Errors, err)
	}
}

func (responseData *ResponseData) AddErrors(errs []error) {
	if errs != nil {
		for _, err := range errs {
			responseData.Errors = append(responseData.Errors, err)
		}
	}
}

func SendResponse(w http.ResponseWriter, data interface{}, status int, errs []error) {
	var sendResponse []byte
	var errorList []string

	if len(errs) > 0 {
		var blankData interface{}
		data = blankData
		errorList = errorsToStrings(errs)

		// If no error code set default to a 400
		if status == 0 || status == 200 {
			status = 400
		}
		// Force a 404 for any record not found errors
		if len(errorList) == 1 && errorList[0] == "record not found" {
			status = 404
		}
	}
	if status == 0 {
		status = 200
	}

	apiResponse := ApiResponse{getMetaData(), data, errorList}
	sendResponse, _ = json.Marshal(apiResponse)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(sendResponse)
	return
}

func SendUnauthorized(w http.ResponseWriter) {
	var sendResponse []byte
	apiResponse := ApiResponse{getMetaData(), EmptyInterface(), []string{"You don't have permssion to this."}}
	sendResponse, _ = json.Marshal(apiResponse)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(403)
	w.Write(sendResponse)
	return
}

func ValidResponse(r *http.Request) bool {
	errList := r.Context().Value("errors").(errors2.ErrorList)
	return len(errList.Errors) == 0
}

func GetRequestUser(r *http.Request) models.User {
	var user models.User
	requestUser := r.Context().Value("user")
	if requestUser != nil {
		user = requestUser.(models.User)
	}
	return user
}

func EmptyInterface() interface{} {
	var empty interface{}
	return empty
}

func (responseData *ResponseData) Send() {
	// send either a good or error return
	if !responseData.Valid() {
		responseData.sendErrorResponse()
		return
	}
	responseData.sendResponse()
}

func (responseData *ResponseData) sendErrorResponse() {
	var data interface{}
	errors := errorsToStrings(responseData.Errors)
	apiResponse := ApiResponse{getMetaData(), data, errors}
	response, _ := json.Marshal(apiResponse)

	// If no error code set default to a 400
	if responseData.Status == 0 {
		responseData.Status = 400
	}

	// Force a 404 for any record not found errors
	if len(errors) == 1 && errors[0] == "record not found" {
		responseData.Status = 404
	}

	responseData.Response.Header().Set("Content-Type", "application/json")
	responseData.Response.WriteHeader(responseData.Status)
	responseData.Response.Write(response)
}

func (responseData *ResponseData) sendResponse() {
	var errors []string
	apiResponse := ApiResponse{getMetaData(), responseData.Data, errors}
	response, _ := json.Marshal(apiResponse)

	if responseData.Status == 0 {
		responseData.Status = 200
	}

	responseData.Response.Header().Set("Content-Type", "application/json")
	responseData.Response.WriteHeader(responseData.Status)
	responseData.Response.Write(response)
}

func getMetaData() interface{} {
	newMeta := MetaResponse{
		"1",
		time.Now(),
	}
	return newMeta
}

func errorsToStrings(errors []error) []string {
	var strings []string
	for _, err := range errors {
		strings = append(strings, err.Error())
	}
	return strings
}

func (responseData *ResponseData) Valid() bool {
	return (len(responseData.Errors) == 0)
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	var errors []string
	apiResponse := ApiResponse{getMetaData(), payload, errors}
	response, _ := json.Marshal(apiResponse)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func RespondWithJSONError(w http.ResponseWriter, code int, errors []string) {
	var data interface{}
	apiResponse := ApiResponse{getMetaData(), data, errors}
	response, _ := json.Marshal(apiResponse)

	// Force a 404 for any record not found errors
	if len(errors) == 1 && errors[0] == "record not found" {
		code = 404
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
