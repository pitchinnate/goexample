package request

import (
	"errors"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func UrlVarInt(r *http.Request, key string) (int, error) {
	value, err := UrlVar(r, key)
	if err != nil {
		return 0, err
	}
	id, err := strconv.Atoi(value)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func UrlVar(r *http.Request, key string) (string, error) {
	vars := mux.Vars(r)
	val, ok := vars[key]
	if !ok {
		return val, errors.New("unable to parse url variable")
	}
	return val, nil
}
