package server

import (
	"meetup/internal/pkg/routes"
	"meetup/internal/utils/db"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

func Run() {
	driver, ok := os.LookupEnv("DB_DRIVER")
	if !ok {
		log.Fatal("DB_DRIVER environment var must be set\n")
	}
	var dbString = ""
	var err error
	if driver == "mysql" {
		dbString, err = db.MysqlString()
	} else {
		dbString, err = db.PostgresString()
	}
	log.Printf("dbString: %s\n", dbString)
	if err != nil {
		log.Fatalf("Error building db string %s\n", err)
	}
	err = db.Init(driver, dbString)
	if err != nil {
		log.Fatalf("Error connecting to the database: %s\n", err)
	}
	defer db.Get().Close()
	runWebServer()
}

func runWebServer() {
	host, ok := os.LookupEnv("HOST")
	if !ok {
		log.Fatalf("missing required env var HOST")
	}
	port, ok := os.LookupEnv("PORT")
	if !ok {
		log.Fatalf("missing required env var PORT")
	}
	fmt.Printf("Running Web Server on  %s:%s\n", host, port)

	router := mux.NewRouter()
	router.StrictSlash(false)
	routes.ApiRoutes(router)
	srv := buildServer(router, host, port)
	log.Fatal(srv.ListenAndServe())
}

func buildServer(router *mux.Router, host string, port string) *http.Server {
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		Debug:            false,
		AllowedHeaders:   []string{"Auth_Token", "Origin", "Accept", "Content-Type", "X-Requested-With"},
		AllowedMethods:   []string{"GET", "POST", "HEAD", "PUT", "DELETE"},
	})
	handler := c.Handler(trimPath(router))

	srv := &http.Server{
		Handler:      handler,
		Addr:         fmt.Sprintf("%s:%s", host, port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	return srv
}

// while this is like middleware we need it to fire before a route is found otherwise you can get 404s
func trimPath(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//log.Printf("request to %s", r.URL.Path)
		if r.URL.Path != "/" {
			r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
			//log.Printf("request modified to %s", r.URL.Path)
		}
		next.ServeHTTP(w, r)
	})
}
