package utils

import (
	"crypto/rand"
	"errors"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"github.com/gorilla/mux"
	"gopkg.in/go-playground/validator.v9"
)

var validate *validator.Validate

func SearchStringSlice(strings []string, search string) bool {
	for _, v := range strings {
		if v == search {
			return true
		}
	}
	return false
}

func SearchInterfaceSlice(values interface{}, search string, key string, filteredItems interface{}) {
	clonedReturn := reflect.ValueOf(filteredItems)
	returnSlice := clonedReturn.Elem()
	clonedInterface := reflect.ValueOf(values)
	internalSlice := clonedInterface.Elem()
	for i := 0; i < internalSlice.Len(); i++ {
		if internalSlice.Index(i).FieldByName(key).String() == search {
			fmt.Println(internalSlice.Index(i))
			returnSlice.Set(reflect.Append(returnSlice, internalSlice.Index(i)))
		}
	}
}

func ContainsInt(numbers []int, search int) bool {
	for _, v := range numbers {
		if v == search {
			return true
		}
	}
	return false
}

func ContainsUInt(numbers []uint, search uint) bool {
	for _, v := range numbers {
		if v == search {
			return true
		}
	}
	return false
}

func UrlVar(r *http.Request, key string) string {
	vars := mux.Vars(r)
	return vars[key]
}

func UrlVarInt(r *http.Request, key string) int {
	value := UrlVar(r, key)
	id, _ := strconv.Atoi(value)
	return id
}

func ErrorsToStrings(errors []error) []string {
	var strings []string
	for _, err := range errors {
		strings = append(strings, err.Error())
	}
	return strings
}

func ValidationErrorsToStrings(errors []validator.FieldError) []string {
	var strings []string
	for _, err := range errors {
		strings = append(strings, fmt.Sprintf("Validation Error on Field: %s Error: %s", err.Field(), err.Tag()))
	}
	return strings
}

func ValidationErrorsToErrors(vErrors []validator.FieldError) []error {
	var returnErrors []error
	for _, err := range vErrors {
		errorString := fmt.Sprintf("Validation Error on Field: %s Error: %s", err.Field(), err.Tag())
		returnErrors = append(returnErrors, errors.New(errorString))
	}
	return returnErrors
}

func GetValidationErrors(model interface{}) []error {
	validate = validator.New()
	validationErr := validate.Struct(model)
	if validationErr == nil {
		return nil
	}
	return ValidationErrorsToErrors(validationErr.(validator.ValidationErrors))
}

func PrintDetails(data interface{}) {
	log.Printf("%#v\n", data)
}

func DeDupeIntSlice(s []int) []int {
	keys := make(map[int]bool)
	var list []int
	for _, item := range s {
		if _, value := keys[item]; !value {
			keys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func FilterIntSlice(s *[]int, q int) {
	filterPos := 0
	for i := 0; i < len(*s); i++ {
		if (*s)[i] == q {
			(*s)[filterPos] = (*s)[i]
			filterPos++
		}
	}
	*s = (*s)[:filterPos]
}

func RandToken(len int) string {
	b := make([]byte, len)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func InterfaceAttributes(incoming interface{}) []string {
	var attributes []string
	val := reflect.Indirect(reflect.ValueOf(incoming))
	PrintDetails(val)
	for i:=0; i<val.NumField();i++{
		attributes = append(attributes, val.Type().Field(i).Name)
	}
	return attributes
}
