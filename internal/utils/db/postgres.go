package db

import (
	"errors"
	"fmt"
	"os"
)

func PostgresString() (string, error) {
	host, ok := os.LookupEnv("DB_HOST")
	if !ok {
		return "", errors.New("missing required env var DB_HOST")
	}
	port, ok := os.LookupEnv("DB_PORT")
	if !ok {
		return "", errors.New("missing required env var DB_PORT")
	}
	user, ok := os.LookupEnv("DB_USER")
	if !ok {
		return "", errors.New("missing required env var DB_USER")
	}
	dbname, ok := os.LookupEnv("DB_DBNAME")
	if !ok {
		return "", errors.New("missing required env var DB_DBNAME")
	}
	password, ok := os.LookupEnv("DB_PASSWORD")
	if !ok {
		return "", errors.New("missing required env var DB_PASSWORD")
	}
	dbString := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s",
		host,
		port,
		user,
		dbname,
		password,
	)
	dbSSLMode, ok := os.LookupEnv("DB_SSLMODE")
	if ok {
		dbString = fmt.Sprintf("%s sslmode=%s", dbString, dbSSLMode)
	}
	return dbString, nil
}
