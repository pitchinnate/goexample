package db

import (
	"errors"
	"fmt"
	"os"
)

func MysqlString() (string, error) {
	host, ok := os.LookupEnv("DB_HOST")
	if !ok {
		return "", errors.New("missing required env var DB_HOST")
	}
	port, ok := os.LookupEnv("DB_PORT")
	if !ok {
		return "", errors.New("missing required env var DB_PORT")
	}
	user, ok := os.LookupEnv("DB_USER")
	if !ok {
		return "", errors.New("missing required env var DB_USER")
	}
	dbname, ok := os.LookupEnv("DB_DBNAME")
	if !ok {
		return "", errors.New("missing required env var DB_DBNAME")
	}
	password, ok := os.LookupEnv("DB_PASSWORD")
	if !ok {
		return "", errors.New("missing required env var DB_PASSWORD")
	}
	dbString := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		user,
		password,
		host,
		port,
		dbname,
	)
	return dbString, nil
}
