package db

import (
	"database/sql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	_ "github.com/lib/pq"
	"log"
)

var (
	db       *gorm.DB
	dbHandle *sql.DB
	setup    bool
)

func Setup() bool {
	return setup
}

func Get() *gorm.DB {
	return db
}

func GetDBHandle() *sql.DB {
	return dbHandle
}

func Init(driver string, url string) error {
	var err error
	//log.Printf("Opening gorm connection with driver: %s and url: %s", driver, url)
	db, err = gorm.Open(driver, url)
	//db.DB().SetMaxIdleConns(5)
	//db.DB().SetMaxOpenConns(100)
	//db.LogMode(true)
	if err != nil {
		log.Printf("%v\n", err)
		return err
	}
	setup = true
	dbHandle = db.DB()

	return nil
}
