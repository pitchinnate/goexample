package models

import "time"

type DefaultModel struct {
	ID        uint       `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `gorm:"DEFAULT:now();" json:"created_at"`
	UpdatedAt time.Time  `gorm:"DEFAULT:now();" json:"updated_at"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}
