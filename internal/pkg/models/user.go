package models

import (
	"encoding/json"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"meetup/internal/utils"
	"meetup/internal/utils/db"
	"meetup/internal/utils/request"
	"net/http"
)

type User struct {
	DefaultModel
	Email      string `json:"email" validate:"required,email"`
	Password   string `json:"-"`
}

type NewUser struct {
	User
	Password string `json:"password" validate:"required,min=8"`
}

type EditUser struct {
	User
	Password string `json:"password,omitempty" validate:"min=8"`
}

func (user *NewUser) Validate() []error {
	return utils.GetValidationErrors(user)
}

func (user *User) Validate() []error {
	return utils.GetValidationErrors(user)
}

func (user *User) CheckPassword(incoming string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(incoming))
	return err == nil
}

func (user *User) HashPassword() error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(user.Password), 14)
	user.Password = string(bytes)
	return err
}

func (user *User) Insert() []error {
	return db.Get().Save(&user).GetErrors()
}

func (user *User) Update(editUser EditUser) []error {
	if editUser.Password != "" {
		user.Password = editUser.Password
		user.HashPassword()
	}
	names := utils.InterfaceAttributes(editUser)
	utils.PrintDetails(names)
	var dataInterface map[string]interface{}
	dataJson, err := json.Marshal(editUser)
	if err != nil {
		return []error{err}
	}
	json.Unmarshal(dataJson, &dataInterface)
	return db.Get().Model(&user).Omit("id").Updates(dataInterface).GetErrors()
}

func (user *User) GetFromUrl(r *http.Request, urlParam string) []error {
	userId, err := request.UrlVarInt(r, "userId")
	if err != nil {
		return []error{err}
	}
	return user.GetById(userId)
}

func (user *User) GetById(id int) []error {
	if errs := db.Get().First(&user, id).GetErrors(); len(errs) > 0 {
		return errs
	}
	if user.ID == 0 {
		return []error{errors.New("User not found")}
	}
	return []error{}
}

func (user *User) Delete() []error {
	return db.Get().Delete(&user).GetErrors()
}
