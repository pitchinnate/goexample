package middleware

import (
	"meetup/internal/utils/response"
	"net/http"
)

func UserMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := response.GetRequestUser(r)
		if user.ID > 0 {
			next.ServeHTTP(w, r)
		} else {
			response.SendUnauthorized(w)
			return
		}
	})
}
