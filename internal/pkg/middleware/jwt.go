package middleware

import (
	"meetup/internal/pkg/models"
	"meetup/internal/utils"
	"meetup/internal/utils/db"
	"meetup/internal/utils/response"
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"os"
	"strconv"
)

func JwtMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("auth_token")
		if tokenString == "" {
			next.ServeHTTP(w, r)
			return
		}

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			secret, ok := os.LookupEnv("ENCRYPT_KEY")
			if !ok {
				log.Fatalf("missing required env var ENCRYPT_KEY")
			}
			hmacSampleSecret := []byte(secret)
			return hmacSampleSecret, nil
		})

		if err != nil {
			fmt.Printf("error parsing jwt: %s", err)
			tokenErrors := []error{err}
			response.RespondWithJSONError(w, 401, utils.ErrorsToStrings(tokenErrors))
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if ok && token.Valid {
			userId, _ := strconv.Atoi(fmt.Sprint(claims["user_id"]))
			var user models.User
			err := db.Get().First(&user, userId).GetErrors()
			if err != nil && len(err) > 0 {
				response.RespondWithJSONError(w, 400, utils.ErrorsToStrings(err))
				return
			}
			//Add data to context
			ctx := context.WithValue(r.Context(), "user", user)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			fmt.Println(err)
			tokenErrors := []error{err}
			response.RespondWithJSONError(w, 401, utils.ErrorsToStrings(tokenErrors))
			return
		}
	})
}
