package middleware

import "net/http"

type Middleware struct{}

// bind each middleware here
func (m *Middleware) Log(next http.Handler) http.Handler {
	return logRequests(next)
}
func (m *Middleware) Jwt(next http.Handler) http.Handler {
	return JwtMiddleware(next)
}
func (m *Middleware) User(next http.Handler) http.Handler {
	return UserMiddleware(next)
}
