package routes

import (
	"meetup/internal/pkg/middleware"
	"meetup/internal/pkg/routes/v1"
	"meetup/internal/utils/response"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func ApiRoutes(router *mux.Router) {
	middleware := middleware.Middleware{}
	router.Use(middleware.Log)
	//router.HandleFunc("/favicon.ico", getFavIcon).Methods("GET")
	router.HandleFunc("/", getHome).Methods("GET")
	v1.AddRoutes(router, &middleware)
	router.NotFoundHandler = http.HandlerFunc(pageNotFound)
}

func getHome(w http.ResponseWriter, r *http.Request) {
	message := response.TextResponse{"API Home"}
	response.RespondWithJSON(w, 200, message)
}

//func getFavIcon(w http.ResponseWriter, r *http.Request) {
//	content, err := assets.Asset("static/favicon.ico")
//	if err != nil {
//		log.Fatal("Error loading /favicon.ico file")
//	}
//	w.Write(content)
//}

func pageNotFound(w http.ResponseWriter, r *http.Request) {
	log.Printf("Page not found: %s\n", r.RequestURI)
	message := response.TextResponse{"PAGE NOT FOUND"}
	response.RespondWithJSON(w, 404, message)
}
