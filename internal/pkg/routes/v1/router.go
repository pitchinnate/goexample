package v1

import (
	"github.com/gorilla/mux"
	"net/http"
	"meetup/internal/pkg/middleware"
	"meetup/internal/pkg/routes/v1/auth"
	"meetup/internal/pkg/routes/v1/users"
	"meetup/internal/utils/response"
)

func AddRoutes(router *mux.Router, middleware *middleware.Middleware) {
	s := router.PathPrefix("/v1").Subrouter()
	s.Use(middleware.Jwt)
	auth.AddRoutes(s, middleware)
	users.AddRoutes(s, middleware)
	s.HandleFunc("", getHome).Methods("GET")
}

func getHome(w http.ResponseWriter, r *http.Request) {
	message := response.TextResponse{"API Version 1"}
	response.RespondWithJSON(w, 200, message)
}
