package auth

import (
	"meetup/internal/pkg/models"
	"meetup/internal/utils"
	"meetup/internal/utils/db"
	"meetup/internal/utils/response"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"log"
	"net/http"
	"os"
	"strconv"
)

type resetForm struct {
	Token    string `json:"token" validate:"required"`
	Password string `json:"password" validate:"required"`
	Email    string `json:"email" validate:"required"`
}

type resetResponse struct {
	Success bool   `json:"success"`
	Token   string `json:"token"`
}

func (resetForm *resetForm) Validate() []error {
	return utils.GetValidationErrors(resetForm)
}

func postReset(w http.ResponseWriter, r *http.Request) {
	var resetForm resetForm
	if err := json.NewDecoder(r.Body).Decode(&resetForm); err != nil {
		response.SendResponse(w, resetForm, 400, []error{err})
		return
	}
	if errs := resetForm.Validate(); len(errs) > 0 {
		response.SendResponse(w, resetForm, 400, errs)
		return
	}

	var user models.User
	errs := db.Get().Where("name = ?", resetForm.Email).First(&user).GetErrors()
	if errs != nil && len(errs) > 0 {
		response.SendResponse(w, resetForm, 400, []error{errors.New("Invalid link - email")})
		return
	}

	tokenString, _ := base64.StdEncoding.DecodeString(resetForm.Token)
	token, err := jwt.Parse(string(tokenString), func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		secret, ok := os.LookupEnv("ENCRYPT_KEY")
		if !ok {
			log.Fatalf("missing required env var ENCRYPT_KEY")
		}
		hmacSampleSecret := []byte(secret)
		return hmacSampleSecret, nil
	})

	if err != nil {
		response.SendResponse(w, response.EmptyInterface(), 400, []error{errors.New("Invalid Reset Link")})
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		response.SendResponse(w, response.EmptyInterface(), 400, []error{errors.New("Invalid Reset Link")})
		return
	}

	userId, _ := strconv.Atoi(fmt.Sprint(claims["user_id"]))
	if user.ID != uint(userId) {
		response.SendResponse(w, response.EmptyInterface(), 400, []error{errors.New("Invalid Reset Link")})
		return
	}

	user.Password = resetForm.Password
	user.HashPassword()
	db.Get().Model(&user).Update(map[string]interface{}{"password": user.Password})

	newToken := generateJwt(user)
	response.SendResponse(w, resetResponse{true, newToken}, 200, []error{})
}
