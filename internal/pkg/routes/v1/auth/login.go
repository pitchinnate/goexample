package auth

import (
	"meetup/internal/pkg/models"
	"meetup/internal/utils"
	"meetup/internal/utils/db"
	"meetup/internal/utils/response"
	"encoding/json"
	"errors"
	"net/http"
)

type loginForm struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type loginResponse struct {
	Token string `json:"token"`
}

func (loginForm *loginForm) Validate() []error {
	return utils.GetValidationErrors(loginForm)
}

func postLogin(w http.ResponseWriter, r *http.Request) {
	var loginForm loginForm
	var user models.User

	if err := json.NewDecoder(r.Body).Decode(&loginForm); err != nil {
		response.SendResponse(w, loginForm, 400, []error{err})
		return
	}
	if errs := loginForm.Validate(); len(errs) > 0 {
		response.SendResponse(w, loginForm, 400, errs)
		return
	}
	if errs := db.Get().Where("email = ?", loginForm.Email).First(&user).GetErrors(); len(errs) > 0 {
		response.SendResponse(w, loginForm, 400, []error{errors.New("Invalid username or password")})
		return
	}
	if validPassword := user.CheckPassword(loginForm.Password); !validPassword {
		response.SendResponse(w, loginForm, 400, []error{errors.New("Invalid username or password")})
		return
	}
	token := generateJwt(user)
	response.SendResponse(w, loginResponse{token}, 200, []error{})
}
