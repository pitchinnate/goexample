package auth

import (
	"meetup/internal/pkg/middleware"
	"meetup/internal/pkg/models"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"log"
	"os"
	"time"
)

func AddRoutes(router *mux.Router, middleware *middleware.Middleware) {
	s := router.PathPrefix("/auth").Subrouter()
	s.HandleFunc("/login", postLogin).Methods("POST")
	s.HandleFunc("/forgot", postForgot).Methods("POST")
	s.HandleFunc("/reset", postReset).Methods("POST")
	s.HandleFunc("/register", postRegister).Methods("POST")
}

func generateJwt(user models.User) string {
	secret, ok := os.LookupEnv("ENCRYPT_KEY")
	if !ok {
		log.Fatalf("missing required env var ENCRYPT_KEY")
	}
	mySigningKey := []byte(secret)

	type MyCustomClaims struct {
		UserId uint   `json:"user_id"`
		Email  string `json:"email"`
		jwt.StandardClaims
	}

	claims := MyCustomClaims{
		user.ID,
		user.Email,
		jwt.StandardClaims{
			ExpiresAt: (int64(time.Now().Unix()) + (3600 * 24 * 60)),
			Issuer:    "meetup.com",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, _ := token.SignedString(mySigningKey)
	return ss
}
