package auth

import (
	"meetup/internal/pkg/models"
	"meetup/internal/utils"
	"meetup/internal/utils/db"
	"meetup/internal/utils/mail"
	"meetup/internal/utils/response"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
)

type forgotForm struct {
	Email       string `json:"email" validate:"required"`
	RedirectUrl string `json:"redirectUrl" validate:"required"`
}

type forgotResponse struct {
	Success bool `json:"success"`
}

func (forgotForm *forgotForm) Validate() []error {
	return utils.GetValidationErrors(forgotForm)
}

func postForgot(w http.ResponseWriter, r *http.Request) {
	var forgotForm forgotForm

	if err := json.NewDecoder(r.Body).Decode(&forgotForm); err != nil {
		response.SendResponse(w, forgotForm, 400, []error{err})
		return
	}
	if errs := forgotForm.Validate(); len(errs) > 0 {
		response.SendResponse(w, forgotForm, 400, errs)
		return
	}

	var user models.User
	err := db.Get().Where("name = ?", forgotForm.Email).First(&user).GetErrors()
	if err != nil && len(err) > 0 {
		sendError := sendBadEmail(forgotForm.Email)
		if sendError != nil {
			response.SendResponse(w, forgotForm, 400, []error{sendError})
			return
		}
	} else {
		sendError := sendGoodEmail(user, forgotForm)
		if sendError != nil {
			response.SendResponse(w, forgotForm, 400, []error{sendError})
			return
		}
	}

	response.SendResponse(w, forgotResponse{true}, 200, []error{})
}

func sendBadEmail(email string) error {
	var message = `<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Forgot Password</title>
	</head>
	<body>
		A forgot password request was made for %s however that email address does not have account with us. If you
		requested the forgot password please try a different email address. If you did not request a forgot password
		you can ignore this email.
	</body>
</html>`

	newMessage := mail.EmailMessage{
		email,
		"no-reply@meetup.com",
		"Forgot Password Request",
		fmt.Sprintf(message, email),
	}
	return mail.SendEmail(newMessage)
}

func sendGoodEmail(user models.User, request forgotForm) error {
	var message = `<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Forgot Password</title>
	</head>
	<body>
		A forgot password request was made for %s. Click the following link or copy and paste it.<br><br>
		<a href='%s'>%s</a>
<br><br>
If you did not request a forgot password you can ignore this email.
	</body>
</html>`

	token := generateJwt(user)
	b64token := base64.StdEncoding.EncodeToString([]byte(token))

	url := fmt.Sprintf("%s?token=%d&email=%s", request.RedirectUrl, b64token, user.Email)

	newMessage := mail.EmailMessage{
		user.Email,
		"no-reply@meetup.com",
		"Forgot Password Request",
		fmt.Sprintf(message, user.Email, url, url),
	}
	return mail.SendEmail(newMessage)
}
