package auth

import (
	"meetup/internal/pkg/models"
	"meetup/internal/utils/response"
	"encoding/json"
	"net/http"
)

func postRegister(w http.ResponseWriter, r *http.Request) {
	var newUser models.NewUser
	var user models.User

	if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
		response.SendResponse(w, response.EmptyInterface(), 400, []error{err})
		return
	}
	if errs := newUser.Validate(); len(errs) > 0 {
		response.SendResponse(w, response.EmptyInterface(), 400, errs)
		return
	}
	userString, _ := json.Marshal(newUser)
	json.Unmarshal(userString, &user)
	user.Password = newUser.Password
	user.HashPassword()
	if errs := user.Insert(); len(errs) > 0 {
		response.SendResponse(w, response.EmptyInterface(), 400, errs)
		return
	}
	token := generateJwt(user)
	response.SendResponse(w, loginResponse{token}, 200, []error{})
}
