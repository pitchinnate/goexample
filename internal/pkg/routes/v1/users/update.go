package users

import (
	"meetup/internal/pkg/models"
	"meetup/internal/utils"
	"meetup/internal/utils/response"
	"encoding/json"
	"net/http"
)

func putUser(w http.ResponseWriter, r *http.Request) {
	var editUser models.EditUser
	var user models.User

	if errs := user.GetById(utils.UrlVarInt(r, "userId")); len(errs) > 0 {
		response.SendResponse(w, user, 400, errs)
		return
	}
	if err := json.NewDecoder(r.Body).Decode(&editUser); err != nil {
		response.SendResponse(w, user, 400, []error{err})
		return
	}
	errs := user.Update(editUser)
	response.SendResponse(w, user, 200, errs)
}
