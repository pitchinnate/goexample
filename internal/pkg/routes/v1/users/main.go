package users

import (
	"meetup/internal/pkg/middleware"
	"github.com/gorilla/mux"
)

func AddRoutes(router *mux.Router, middleware *middleware.Middleware) {
	s := router.PathPrefix("/users").Subrouter()
	//s.Use(middleware.User)
	s.HandleFunc("/{userId}", putUser).Methods("PUT")
	s.HandleFunc("/{userId}", getUser).Methods("GET")
	s.HandleFunc("", createUser).Methods("POST")
	s.HandleFunc("", getUsers).Methods("GET")
	s.HandleFunc("/{userId}", deleteUser).Methods("DELETE")
}
