package users

import (
	"meetup/internal/pkg/models"
	"meetup/internal/utils/db"
	"meetup/internal/utils/response"
	"net/http"
)

func getUsers(w http.ResponseWriter, r *http.Request) {
	var users []models.User
	if errs := db.Get().Find(&users).GetErrors(); errs != nil && len(errs) > 0 {
		response.SendResponse(w, response.EmptyInterface(), 400, errs)
		return
	}
	response.SendResponse(w, users, 200, []error{})
}
