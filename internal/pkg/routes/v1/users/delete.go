package users

import (
	"meetup/internal/pkg/models"
	"meetup/internal/utils/response"
	"net/http"
)

func deleteUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	if errs := user.GetFromUrl(r, "userId"); errs != nil && len(errs) > 0 {
		response.SendResponse(w, user, 404, errs)
		return
	}
	user.Delete()
	var responseText interface{}
	responseText = "User Deleted"
	response.SendResponse(w, responseText, 201, []error{})
}
