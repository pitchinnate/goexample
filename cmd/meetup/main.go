package main

import (
	"meetup/internal/utils/server"
	"flag"
	"github.com/joho/godotenv"
	"log"
	_ "net/http/pprof"
)

func main() {
	var isProduction bool
	flag.BoolVar(&isProduction, "prod", false, "App is running in production")
	flag.Parse()
	if !isProduction {
		loadConfig()
	}
	server.Run()
}

func loadConfig() {
	log.Println("See if we can access an .env file directory")
	err := godotenv.Load("../../static/configs/.env")
	if err != nil {
		log.Fatal(err)
		//loadViaAsset()
	}
}
