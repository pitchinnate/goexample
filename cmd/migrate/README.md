# Migrations

Migrations are handled by [goose](https://github.com/pressly/goose).

To create a migration do

```bash
$ ./goose create change_something_in_the_db.go
$ Created new file: 00003_change_something_in_the_db.go
```

To run a `.go` migration you first have to build the custom binary.

After writing your migration file in this directory (see `example_migration.go`), rebuild the binary.

```bash
$ go build -o goose *.go
```
Or on Windows
```bash
go build -o goose.exe
```

Then run migrations using the executable (add .exe to the executable if on windows)

```bash
$ ./goose postgres "dbname=mydatabase user=postgres password=mypassword host=localhost sslmode=disable" up
$ Opening gorm connection with url: dbname=mydatabase user=postgres password=mypassword host=localhost sslmode=disable
$ OK    00001_original_user.go
$ goose: no migrations to run. current version: 1
```
