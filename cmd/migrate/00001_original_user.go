package main

import (
	"database/sql"
	"meetup/internal/utils/db"
	"meetup/internal/pkg/models"
	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up00001, Down00001)
}

type User struct {
	models.DefaultModel
	Email           string `gorm:"not null;unique_index:user_email"`
	Password        string `gorm:"not null"`
}

func Up00001(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	db.Get().CreateTable(&User{})
	return nil
}

func Down00001(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	db.Get().DropTable(&User{})
	return nil
}
